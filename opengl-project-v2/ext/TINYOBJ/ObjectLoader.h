#ifndef OBJECT_LOADER_H
#define OBJECT_LOADER_H

#include "tiny_obj_loader.h"

using namespace tinyobj;

struct ObjectData;

class ObjectLoader
{
public:

	static ObjectData* LoadObject(char* _filePath);
};

#endif