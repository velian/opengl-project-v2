#include "TINYOBJ/ObjectLoader.h"
#include "TINYOBJ/ObjectData.h"

ObjectData* ObjectLoader::LoadObject(char* _filePath)
{
	ObjectData* data = new ObjectData;

	std::string error = tinyobj::LoadObj(data->shapes, data->materials, _filePath);

	if (!error.empty())
	{
		printf("Error during object load : %s", error);
		return data;
	}

	return data;
}