#ifndef OBJECT_H
#define OBJECT_H

#include "Scene/Shader.h"

struct Vertex;
struct ObjectData;
class FreeCamera;
class Texture;
class GameState;
class Light;
class Shadow;

class Object
{
public:
	Object();

	void Initialize(char* _filePath);
	void Initialize(char* _filePath, Light* _light);
	void Initialize(char* _objPath, char* _texPath);
	void Initialize(char* _objPath, char* _texPath, Light* _light);

	void CreatePlane();

	virtual void Update();
	void Draw(FreeCamera* _camera, Shadow* _shadow = nullptr);

	void DrawGUI(char* _objectName);

	ObjectData* GetData(){ return m_data; }

	Texture* GetTexture(){ return m_texture; }
	void SetTexture(Texture* _texture){ m_texture = _texture; }

	Texture* GetOverlay(){ return m_textureOverlay; }
	void SetOverlay(Texture* _texture){ m_textureOverlay = _texture; }

	Light* GetLight() { return m_light; }

	ShaderData* GetShaderData() { return m_shaderData; }

	void OpenDialog(); //Open a dialog to select object and texture

private:

	void Create(bool _loadShaders = true);

	void InitializeLight(Light* _light);

	ObjectData* m_data;
	ShaderData* m_shaderData;
	Shader* m_shader;
	Texture* m_texture;
	Texture* m_textureOverlay;
	Light* m_light;

	ShaderData* m_planeData;
};


#endif