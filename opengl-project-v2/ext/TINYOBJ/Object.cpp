#include <assert.h>

#include <IMGUI/imgui.h>

#include "TINYOBJ/Object.h"
#include "TINYOBJ/ObjectLoader.h"
#include "TINYOBJ/ObjectData.h"
#include "Scene/Shader.h"
#include "Scene/Camera.h"
#include "Scene/Texture.h"
#include "GameState/GameState.h"
#include "Scene/Light.h"
#include "Scene/Vertex.h"
#include "Scene/Shadow.h"

Object::Object() :
m_light(nullptr)
{

}
void Object::Initialize(char* _objPath)
{
	Initialize(_objPath, nullptr, nullptr);
}

void Object::Initialize(char* _objPath, Light* _light)
{
	Initialize(_objPath, nullptr, _light);
}

void Object::Initialize(char* _objPath, char* _texPath)
{
	Initialize(_objPath, _texPath, nullptr);
}

void Object::Initialize(char* _objPath, char* _texPath, Light* _light)
{
	m_texture = new Texture();
	m_texture->Initialize(_texPath, LINEAR);

	InitializeLight(_light);

	m_data = ObjectLoader::LoadObject(_objPath);
	m_shader = new Shader();
	m_shaderData = new ShaderData();

	m_data->m_objectShapeSize = m_data->shapes.size();

	Create();
}

void Object::InitializeLight(Light* _light)
{
	//assert(m_light == nullptr);

	if (_light == nullptr)
	{
		m_light->m_specular = 0.f;
		m_light->m_ambientLight = 1.f;
		m_light->m_lightDirection = glm::vec3(45, 45, 45);
		m_light->m_lightColour = glm::vec3(1, 1, 1);
		printf("Light Set To Default In Object");
	}
	else
	{
		m_light = _light;
	}

}

void Object::CreatePlane()
{
	m_planeData = new ShaderData();

	float vertexData[] = {
		-5, 0,  5, 0, 1, 0, 0, 0,
		 5, 0,  5, 0, 1, 0, 0, 0,
		 5, 0, -5, 0, 1, 0, 0, 0,
		-5, 0, -5, 0, 1, 0, 0, 0,
	};
	unsigned int indexData[] = {
		0, 1, 2,
		0, 2, 3,
	};
	glGenVertexArrays(1, &m_planeData->m_VAO);
	glBindVertexArray(m_planeData->m_VAO);
	glGenBuffers(1, &m_planeData->m_VBO);

	glBindBuffer(GL_ARRAY_BUFFER, m_planeData->m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)* 8 * 4,	vertexData, GL_STATIC_DRAW);

	glGenBuffers(1, &m_planeData->m_IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_planeData->m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int)* 6, indexData, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,	sizeof(float)* 8, 0);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,	sizeof(float)* 8, ((char*)0) + 12);

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(float)* 8, ((char*)0) + 24);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Object::Create(bool _loadShaders)
{	
	if (_loadShaders){
		m_shader->LoadProgram(nullptr, "./shader/objMain.vs", "./shader/objMain.fs");
	}

	CreatePlane();

	for (unsigned int i = 0; i < m_data->shapes.size(); ++i)
	{
		glGenVertexArrays(1, &m_shaderData[i].m_VAO);
		glGenBuffers(1, &m_shaderData[i].m_VBO);
		glGenBuffers(1, &m_shaderData[i].m_IBO);
		glBindVertexArray(m_shaderData[i].m_VAO);

		unsigned int floatCount = m_data->shapes[i].mesh.positions.size();
		floatCount += m_data->shapes[i].mesh.normals.size();
		floatCount += m_data->shapes[i].mesh.texcoords.size();

		std::vector<float> vertexData;
		vertexData.reserve(floatCount);

		vertexData.insert(vertexData.end(),
			m_data->shapes[i].mesh.positions.begin(),
			m_data->shapes[i].mesh.positions.end());

		vertexData.insert(vertexData.end(),
			m_data->shapes[i].mesh.normals.begin(),
			m_data->shapes[i].mesh.normals.end());

		vertexData.insert(vertexData.end(),
			m_data->shapes[i].mesh.texcoords.begin(),
			m_data->shapes[i].mesh.texcoords.end());

		GetData()[i].m_indexCount = m_data->shapes[i].mesh.indices.size();

		glBindBuffer(GL_ARRAY_BUFFER, m_shaderData[i].m_VBO);
		glBufferData(GL_ARRAY_BUFFER, 
			vertexData.size() * sizeof(float),
			vertexData.data(), GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_shaderData[i].m_IBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_data->shapes[i].mesh.indices.size() * sizeof(unsigned int),
			m_data->shapes[i].mesh.indices.data(), GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 0, (void*)(sizeof(float)*m_data->shapes[i].mesh.positions.size()));
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(float)*(m_data->shapes[i].mesh.positions.size() + (m_data->shapes[i].mesh.normals.size()))));
		
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}
}

void Object::Update()
{
	m_light->CalculateProjection();
}

void Object::Draw(FreeCamera* _camera, Shadow* _shadow)
{
	glUseProgram(m_shader->m_programID);

	int location = glGetUniformLocation(m_shader->m_programID, "ProjectionView");
	glUniformMatrix4fv(location, 1, GL_FALSE, (float*)&_camera->GetProjectionView()[0][0]);

	if (_shadow != nullptr)
	{
		glm::mat4 textureSpaceOffset
		(
			0.5f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.5f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.5f, 0.0f,
			0.5f, 0.5f, 0.5f, 1.0f
		);

		location = glGetUniformLocation(m_shader->m_programID, "HasShadow");
		glUniform1i(location, true);

		glm::mat4 lightMatrix = textureSpaceOffset * m_light->m_lightMatrix;

		location = glGetUniformLocation(m_shader->m_programID, "ShadowMatrix");
		glUniformMatrix4fv(location, 1, GL_FALSE, &lightMatrix[0][0]);

		location = glGetUniformLocation(m_shader->m_programID, "shadowBias");
		glUniform1f(location, 0.01f);

		glActiveTexture(GL_TEXTURE1);		glBindTexture(GL_TEXTURE_2D, _shadow->m_FBODepth);

		location = glGetUniformLocation(m_shader->m_programID, "shadowMap");
		glUniform1i(location, 1);		
	}
	else
	{
		location = glGetUniformLocation(m_shader->m_programID, "HasShadow");
		glUniform1i(location, false);
	}

	if (m_texture != nullptr)
	{
		location = glGetUniformLocation(m_shader->m_programID, "HasTexture");

		glUniform1i(location, true);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_texture->GetID());

		location = glGetUniformLocation(m_shader->m_programID, "diffuse");
		glUniform1i(location, 0);		
	}
	else
	{
		location = glGetUniformLocation(m_shader->m_programID, "HasTexture");
		glUniform1i(location, false);
	}

	if (m_light != nullptr)
	{
		location = glGetUniformLocation(m_shader->m_programID, "HasLight");
		glUniform1i(location, true);

		location = glGetUniformLocation(m_shader->m_programID, "CameraPos");
		glUniform3f(location, _camera->GetPosition().x, _camera->GetPosition().y, _camera->GetPosition().z);

		location = glGetUniformLocation(m_shader->m_programID, "SpecPow");
		glUniform1f(location, m_light->m_specular / 100);
		
		location = glGetUniformLocation(m_shader->m_programID, "ShadowColour");
		glUniform3f(location, m_light->m_shadowColour.x, m_light->m_shadowColour.y, m_light->m_shadowColour.z);
		
		location = glGetUniformLocation(m_shader->m_programID, "LightColour");
		glUniform3f(location, m_light->m_lightColour.x, m_light->m_lightColour.y, m_light->m_lightColour.z);
		
		location = glGetUniformLocation(m_shader->m_programID, "LightDir");
		glUniform3f(location, m_light->m_lightDirection.x, m_light->m_lightDirection.y, m_light->m_lightDirection.z);		
	}																																	   
	else
	{
		location = glGetUniformLocation(m_shader->m_programID, "HasLight");
		glUniform1i(location, false);
	}

	for (unsigned int i = 0; i < GetData()->m_objectShapeSize; i++)
	{
		glBindVertexArray(GetShaderData()[i].m_VAO);
		glDrawElements(GL_TRIANGLES, GetData()->m_indexCount, GL_UNSIGNED_INT, 0);
	}	

	glBindVertexArray(m_planeData->m_VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);

	glUseProgram(0);
}

void Object::DrawGUI(char* _objectName){
	if (ImGui::TreeNode(_objectName)){
		if (m_light != nullptr){
			if (ImGui::TreeNode("Light")){
				float specPow = m_light->m_specular;
				float lightColour[3] = { m_light->m_lightColour.x, m_light->m_lightColour.y, m_light->m_lightColour.z }; // Format the variables for ImGui
				float shadowColour[3] = { m_light->m_shadowColour.x, m_light->m_shadowColour.y, m_light->m_shadowColour.z };
				float lightDirection[3] = { m_light->m_lightDirection.x, m_light->m_lightDirection.y, m_light->m_lightDirection.z };
				
				ImGui::SliderFloat("Specular", &specPow, 0, 100);
				ImGui::SliderFloat3("Direction", lightDirection, -2, 2, "%.3f", 0.5f);
				ImGui::ColorEdit3("A - Colour", lightColour);
				ImGui::ColorEdit3("S - Colour", shadowColour);
				ImGui::TreePop();

				m_light->m_specular = specPow;
				m_light->m_lightDirection.xyz = glm::vec3(lightDirection[0], lightDirection[1], lightDirection[2]);
				m_light->m_shadowColour.xyz = glm::vec3(shadowColour[0], shadowColour[1], shadowColour[2]);
				m_light->m_lightColour.xyz = glm::vec3(lightColour[0], lightColour[1], lightColour[2]);
			}
		}
		ImGui::TreePop();
	}
}

void Object::OpenDialog(){
	OPENFILENAME objectDialog;
	OPENFILENAME textureDialog;
	char szFileName[MAX_PATH] = "";

	ZeroMemory(&objectDialog, sizeof(objectDialog));
	ZeroMemory(&textureDialog, sizeof(textureDialog));

	objectDialog.lStructSize = sizeof(objectDialog);
	textureDialog.lStructSize = sizeof(textureDialog);

	objectDialog.lpstrFile = szFileName;
	objectDialog.nMaxFile = MAX_PATH;
	objectDialog.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;

	textureDialog.lpstrFile = szFileName;
	textureDialog.nMaxFile = MAX_PATH;
	textureDialog.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
	//ofn.lpstrDefExt = L"txt";

	std::string objectPath;
	std::string texturePath;

	if (GetOpenFileName(&objectDialog))
	{
		objectPath = objectDialog.lpstrFile;

		printf("Loaded Object From File: %s\n", objectPath.c_str());
	}
	else
		return;

	if (GetOpenFileName(&textureDialog))
	{
		texturePath = textureDialog.lpstrFile;

		printf("Loaded Texture From File: %s\n", texturePath.c_str());
	}
	else
		return;

	m_data = ObjectLoader::LoadObject((char*)objectPath.c_str());
	m_texture->Initialize(texturePath, DRAW_TYPE::LINEAR);

	m_data->m_objectShapeSize = m_data->shapes.size();

	Create(false);
}