#include <GL/gl_core_4_4.h>
#include <GLFW/glfw3.h>

#include <GLM/gtc/type_ptr.hpp>

#include "FBX/FBXObject.h"
#include "GameState/GameState.h"

#include "Scene/Light.h"
#include "Scene/Texture.h"
#include "Scene/Camera.h"


FBXObject::FBXObject()
{
	Initialize();
}

void FBXObject::Initialize()
{
	m_file = new FBXFile();
	m_shader = new Shader();
	m_light = nullptr;

	m_shader->Initialize();

	m_timer = 0;

}

void FBXObject::Create(const char* a_filename, FBXFile::UNIT_SCALE a_scale, bool a_loadTextures, bool a_loadAnimations, bool a_flipTextureY)
{
	m_file->load(a_filename, a_scale, a_loadTextures, a_loadAnimations, a_flipTextureY);

	if (m_file == NULL)
	{
		printf("Failed to load FBX Object : %s", a_filename);
		return;
	}

	m_shader->LoadProgram(nullptr, "./shader/fbxMain.vs", "./shader/fbxMain.fs");

	for (unsigned int i = 0; i < m_file->getMeshCount(); ++i)
	{
		FBXMeshNode* pMesh = m_file->getMeshByIndex(i);

		ShaderData* meshData = new ShaderData();

		glGenVertexArrays(1, &(meshData->m_VAO));
		glGenBuffers(1, &(meshData->m_VBO));
		glGenBuffers(1, &(meshData->m_IBO));

		glBindVertexArray(meshData->m_VAO);

		glBindBuffer(GL_ARRAY_BUFFER, meshData->m_VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(FBXVertex)* pMesh->m_vertices.size(), pMesh->m_vertices.data(), GL_STATIC_DRAW);

		glEnableVertexAttribArray(0); //Position
		glEnableVertexAttribArray(1); //Indices
		glEnableVertexAttribArray(2); //Weights
		glEnableVertexAttribArray(3); //Normal
		glEnableVertexAttribArray(4); //Texcoord

		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), ((char*)0) + FBXVertex::PositionOffset);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), ((char*)0) + FBXVertex::IndicesOffset);
		glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), ((char*)0) + FBXVertex::WeightsOffset);
		glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), ((char*)0) + FBXVertex::NormalOffset);
		glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), ((char*)0) + FBXVertex::TexCoord1Offset);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, meshData->m_IBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int)* pMesh->m_indices.size(), pMesh->m_indices.data(), GL_STATIC_DRAW);

		glBindVertexArray(0);

		pMesh->m_userData = (void*)meshData;
	}

	if (m_file->getTextureCount() > 0)
	{
		m_file->initialiseOpenGLTextures();
	}
	
}

void FBXObject::Update(float _deltaTime)
{
	if (m_file->getSkeletonCount() <= 0)
	{
		return;
	}

	// grab the skeleton and animation we want to use
	FBXSkeleton* skeleton = m_file->getSkeletonByIndex(0);
	FBXAnimation* animation = m_file->getAnimationByIndex(0);

	m_timer += _deltaTime;

	// evaluate the animation to update bones
	skeleton->evaluate(animation, m_timer);

	for (unsigned int bone_index = 0; bone_index < skeleton->m_boneCount; ++bone_index)
	{
		skeleton->m_nodes[bone_index]->updateGlobalTransform();
	}
}

void FBXObject::Draw(FreeCamera* _camera)
{
	glUseProgram(m_shader->m_programID);

	unsigned int location = glGetUniformLocation(m_shader->m_programID, "ProjectionView");
	glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(_camera->GetProjectionView()));

	location = glGetUniformLocation(m_shader->m_programID, "CameraPos");
	glUniform3f(location, _camera->GetPosition().x, _camera->GetPosition().y, _camera->GetPosition().z);

	if (GetFile()->getSkeletonCount() > 0)
	{
		FBXSkeleton* skeleton = GetFile()->getSkeletonByIndex(0);
		skeleton->updateBones();

		for (unsigned int i = 0; i < skeleton->m_boneCount; i++)
		{
			std::string boneName = "Bones[" + std::to_string(i) + "]";
			location = glGetUniformLocation(m_shader->m_programID, boneName.c_str());
			glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(skeleton->m_bones[i]));
		}
	}

	if (GetLight() != nullptr)
	{
		location = glGetUniformLocation(m_shader->m_programID, "SpecPow");
		glUniform1f(location, m_light->m_specular / 100);

		location = glGetUniformLocation(m_shader->m_programID, "AmbientLight");
		glUniform1f(location, m_light->m_ambientLight / 100);

		location = glGetUniformLocation(m_shader->m_programID, "LightColour");
		glUniform3f(location, m_light->m_lightColour.x, m_light->m_lightColour.y, m_light->m_lightColour.z);

		location = glGetUniformLocation(m_shader->m_programID, "LightDir");
		glUniform3f(location, -m_light->m_lightDirection.x, -m_light->m_lightDirection.y, -m_light->m_lightDirection.z);
	}

	for (unsigned int i = 0; i < GetFile()->getMeshCount(); i++)
	{
		FBXMeshNode* pMesh = GetFile()->getMeshByIndex(i);
		ShaderData* pMeshData = (ShaderData*)pMesh->m_userData;

		location = glGetUniformLocation(m_shader->m_programID, "global");
		glUniformMatrix4fv(location, 1, GL_FALSE, (float*)&pMesh->m_globalTransform[0][0]);

		if (pMesh->m_material->textures[FBXMaterial::DiffuseTexture] != nullptr)
		{
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, pMesh->m_material->textures[FBXMaterial::DiffuseTexture]->handle);
		}

		if (pMesh->m_material->textures[FBXMaterial::NormalTexture] != nullptr)
		{
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, pMesh->m_material->textures[FBXMaterial::NormalTexture]->handle);
		}

		if (pMesh->m_material->textures[FBXMaterial::SpecularTexture] != nullptr)
		{
			glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_2D, pMesh->m_material->textures[FBXMaterial::SpecularTexture]->handle);
		}

		if (pMesh->m_material->textures[FBXMaterial::GlowTexture] != nullptr)
		{
			glActiveTexture(GL_TEXTURE3);
			glBindTexture(GL_TEXTURE_2D, pMesh->m_material->textures[FBXMaterial::GlowTexture]->handle);
		}

		if (pMesh->m_material->textures[FBXMaterial::GlossTexture] != nullptr)
		{
			glActiveTexture(GL_TEXTURE4);
			glBindTexture(GL_TEXTURE_2D, pMesh->m_material->textures[FBXMaterial::GlossTexture]->handle);
		}

		glBindVertexArray(pMeshData->m_VAO);
		glDrawElements(GL_TRIANGLES, (unsigned int)pMesh->m_indices.size(), GL_UNSIGNED_INT, 0);
	}

	glBindVertexArray(0);
	glUseProgram(0);

}