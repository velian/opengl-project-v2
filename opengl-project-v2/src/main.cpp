#include "Application.h"

int main(int argc, char **argv){
	Application* application = new Application(1280, 720, "opengl-project-v2");
	application->Run();
	application->Shutdown();
	return 0;
}