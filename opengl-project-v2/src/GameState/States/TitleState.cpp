#include <GL/gl_core_4_4.h>
#include <GLFW/glfw3.h>

#include <iostream>

#include "GameState/States/TitleState.h"
#include "GameState/GameStateManager.h"

TitleState::TitleState(GLFWwindow* _window, GameStateManager* _manager, unsigned int _id) : GameState(_window, _manager, _id){
	
}

void TitleState::Update(float _deltaTime){
	
}

void TitleState::Draw(){
	DrawGUI("TITLE STATE");
}