#include <GL/gl_core_4_4.h>
#include <GLFW/glfw3.h>

#include <iostream>

#include <IMGUI/imgui.h>
#include <IMGUI/imgui_impl_glfw_gl3.h>

#include "GameState/States/TestState.h"
#include "GameState/GameStateManager.h"

#include "FBX/FBXObject.h"
#include "TINYOBJ/Object.h"
#include "Scene/Camera.h"
#include "Scene/Light.h"
#include "Scene/RenderTarget.h"
#include "Scene/Texture.h"
#include "Scene/Skybox.h"
#include "Scene/Shadow.h"

TestState::TestState(GLFWwindow* _window, GameStateManager* _manager, unsigned int _id) : GameState(_window, _manager, _id){
	m_camera = new FreeCamera(10.f, 0.5f);
	m_camera->SetInputWindow(m_window);
	m_camera->SetupPerspective(3.14159f * 0.25f, 4.0f / 3.0f);
	m_camera->SetPosition(glm::vec3(15, 1, 15));
	m_camera->LookAt(glm::vec3(50), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

	std::vector<const GLchar*> filePaths;
	filePaths.push_back("./images/skybox/right.jpg");
	filePaths.push_back("./images/skybox/left.jpg");
	filePaths.push_back("./images/skybox/top.jpg");
	filePaths.push_back("./images/skybox/top.jpg");
	filePaths.push_back("./images/skybox/back.jpg");
	filePaths.push_back("./images/skybox/front.jpg");
	m_skybox = new Skybox(filePaths);

	m_renderTarget = new RenderTarget(POST_TYPE::NONE);
	
	//m_fbxObject = new FBXObject();
	//m_fbxObject->Create("./fbx/marksman/marksman.fbx");

	m_object = new Object();
	m_object->Initialize("./objects/static/alienplanet.obj", "./objects/static/alienplanet.png", new Light(glm::vec3(2,2,2)));

	m_shadow = new Shadow();
	m_shadow->Initialize();

	//m_fbxObject->SetLight(new Light(glm::vec3(2, 2, 2)));
}

void TestState::Update(float _deltaTime){
	if (ImGui::IsKeyPressed(GLFW_KEY_F5, false)){
		m_object->GetTexture()->OpenDialogue();
	}

	//ImGui::InputText("'ENTER' to submit", )
	
	m_camera->Update((double)_deltaTime);
	m_object->Update();
	m_shadow->Update();
	//m_fbxObject->Update(_deltaTime);

	m_renderTarget->Update(m_camera);
}

void TestState::Draw(){
	DrawGUI("TEST STATE");

	m_shadow->Draw(m_camera, m_object);

	m_renderTarget->SetAsActiveFrameBuffer();
	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_skybox->Draw(m_camera);

	//m_fbxObject->Draw(m_camera);	
	m_object->Draw(m_camera, m_shadow); 
	m_object->DrawGUI("Planet Object");

	m_renderTarget->ClearAsActiveFrameBuffer();
	m_renderTarget->Draw(m_camera);
	
}

void TestState::DrawGUI(char* _stateName)
{
	GameState::DrawGUI(_stateName);

	//Custom STATE GUI code goes here	
	if (ImGui::Button("OpenDiag"))
	{
		m_object->OpenDialog();
	}
	//
}