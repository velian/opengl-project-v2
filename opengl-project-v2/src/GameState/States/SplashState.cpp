#include <GL/gl_core_4_4.h>
#include <GLFW/glfw3.h>

#include <iostream>

#include "GameState/States/SplashState.h"
#include "GameState/GameStateManager.h"

SplashState::SplashState(GLFWwindow* _window, GameStateManager* _manager, unsigned int _id) : GameState(_window, _manager, _id){
	
}

void SplashState::Update(float _deltaTime){
	
}

void SplashState::Draw(){	
	DrawGUI("SPLASH STATE");
}