#include "GameState/GameState.h"
#include "GameState/GameStateManager.h"
#include <IMGUI/imgui.h>
#include <IMGUI/imgui_impl_glfw_gl3.h>

GameState::GameState(GLFWwindow* _window, GameStateManager* _manager, unsigned int _id){
	m_window = _window;
	m_manager = _manager;
	m_id = _id;
	m_manager->AddState(this);
}

void GameState::Update(float _deltaTime){

}

void GameState::Draw(){
	
}

void GameState::DrawGUI(char* _stateName){	
	if (ImGui::CollapsingHeader("State Settings"))
	{
		ImGui::SameLine();
		ImGui::Text("CURRENT STATE : %s", _stateName);
		
		if (ImGui::Button("PREVIOUS STATE")){
			m_manager->PreviousState();
		}
		ImGui::SameLine();
		if (ImGui::Button("NEXT STATE")){
			m_manager->NextState();
		}
	}
	ImGui::Separator();
}