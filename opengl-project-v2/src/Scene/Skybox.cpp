#include "Scene/Skybox.h"
#include "Scene/Shader.h"
#include "Scene/Texture.h"
#include "Scene/Camera.h"

Skybox::Skybox(std::vector<const GLchar*> _filePaths)
{
	Initialize(_filePaths);
}

void Skybox::Initialize(std::vector<const GLchar*> _filePaths)
{
	m_shader = new Shader();
	m_shaderData = new ShaderData();
	m_texture = new Texture[6];

	m_texture->LoadCubeMap(_filePaths);

	Create();
}

void Skybox::Create()
{
	m_shader->LoadProgram(nullptr, "./shader/skyboxMain.vs", "./shader/skyboxMain.fs");

	GLfloat skyboxVertices[] = {
		// Positions          
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,

		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,

		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,

		-1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f
	};

	glGenVertexArrays(1, &m_shaderData->m_VAO);
	glGenBuffers(1, &m_shaderData->m_VBO);
	glBindVertexArray(m_shaderData->m_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, m_shaderData->m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glBindVertexArray(0);
}

void Skybox::Draw(FreeCamera* _camera)
{
	glDepthMask(GL_FALSE);
	glUseProgram(m_shader->m_programID);

	int loc = glGetUniformLocation(m_shader->m_programID, "projectionView");
	glm::mat4 view = glm::mat4(glm::mat3(_camera->GetView()));
	glm::mat4 projection = _camera->GetProjection();
	glm::mat4 projectionView = projection * view;

	glUniformMatrix4fv(loc, 1, false, &projectionView[0][0]);
	glBindVertexArray(m_shaderData->m_VAO);
	glActiveTexture(GL_TEXTURE0);
	glUniform1i(glGetUniformLocation(m_shader->m_programID, "skybox"), 0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_texture->GetID());
	glDrawArrays(GL_TRIANGLES, 0, 36);

	glBindVertexArray(0);
	glDepthMask(GL_TRUE);
}