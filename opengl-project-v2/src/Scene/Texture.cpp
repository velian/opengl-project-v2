#include "Scene/Texture.h"
#include "Scene/Camera.h"
#include "Scene/Shader.h"
#include <STB/stb_image.h>

Texture::Texture()
{
	m_shader = new Shader();
}

void Texture::Initialize(std::string _filePath, DRAW_TYPE _drawType)
{
	m_data = stbi_load(_filePath.c_str(), &m_imageWidth, &m_imageHeight, &m_imageFormat, STBI_default);

	m_currentPath = _filePath;

	glGenTextures(1, &m_id);
	glBindTexture(GL_TEXTURE_2D, m_id);

	TEX_TYPE textureType;

	std::transform(m_currentPath.begin(), m_currentPath.end(), m_currentPath.begin(), ::tolower);

	if (m_currentPath.substr(m_currentPath.find_last_of(".") + 1) == "jpg")
		textureType = JPG;

	else if (m_currentPath.substr(m_currentPath.find_last_of(".") + 1) == "png")
		textureType = PNG;

	else if (m_currentPath.substr(m_currentPath.find_last_of(".") + 1) == "tga")
		textureType = TGA;

	else
	{
		printf("Error loading texture file : %s", _filePath);
		return;
	}

	switch (textureType)
	{
		case(JPG) : glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,  m_imageWidth, m_imageHeight, 0, GL_RGB,  GL_UNSIGNED_BYTE, m_data); break;
		case(PNG) : glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_imageWidth, m_imageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, m_data); break;
		case(TGA) : glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,  m_imageWidth, m_imageHeight, 0, GL_RGB,  GL_UNSIGNED_BYTE, m_data); break;
	}
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, _drawType == 0 ? GL_NEAREST : GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, _drawType == 0 ? GL_NEAREST : GL_LINEAR);

	stbi_image_free(m_data);
}

void Texture::Initialize(std::string _filePath, int _frameWidth, int _frameHeight, float _startTime, DRAW_TYPE _drawType)
{
	m_spriteSheet = true;
	m_frameWidth = _frameWidth, m_frameHeight = _frameHeight;
	m_startTime = _startTime;
	m_timer = _startTime;

	m_uv = glm::vec2(0, 0);

	Initialize(_filePath, _drawType);

	m_maxFrames = m_imageWidth / _frameWidth;
}

void Texture::OpenDialogue(DRAW_TYPE _drawType)
{
	OPENFILENAME dialog;

	char szFileName[MAX_PATH] = "";

	ZeroMemory(&dialog, sizeof(dialog));

	dialog.lStructSize = sizeof(dialog);

	dialog.lpstrFilter = "PNG File (*.png)\0*.png\0JPG File (*jpg)\0*.jpg\0TGA File (*.tga)\0*.tga";
		
	dialog.lpstrFile = szFileName;
	dialog.nMaxFile = MAX_PATH;
	dialog.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST;
	//ofn.lpstrDefExt = L"txt";

	std::string texturePath;

	if (GetOpenFileName(&dialog))
	{
		texturePath = dialog.lpstrFile;

		printf("Loaded Texture From File: %s", texturePath.c_str());
	}

	Initialize((char*)texturePath.c_str(), _drawType);
}

void Texture::Update(float _deltaTime)
{
	if (m_spriteSheet == true && m_timer < 0)
	{
		m_timer = m_startTime;

		m_currentFrame++;

		if (m_currentFrame > m_maxFrames - 1)
		{
			m_currentFrame = 0;
		}
	}
	else
	{
		m_timer -= _deltaTime;
	}
}

void Texture::Draw(FreeCamera* _camera)
{
	
}

void Texture::LoadCubeMap(std::vector<const GLchar*> faces)
{
	glGenTextures(1, &m_id);
	glActiveTexture(GL_TEXTURE0);

	int width, height;
	unsigned char* image;

	glBindTexture(GL_TEXTURE_CUBE_MAP, m_id);

	for (GLuint i = 0; i < faces.size(); i++)
	{
		image = stbi_load(faces[i], &width, &height, 0, STBI_default);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	}

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}