#include "Scene/SoundManager.h"
#include "Scene/Sound.h"
#include "Scene/Sound3D.h"
#include "Scene/Camera.h"

SoundManager::SoundManager()
{
	Initialize();
}

void SoundManager::Initialize()
{
	System_Create(&m_system);
	m_system->init(512, FMOD_INIT_3D_RIGHTHANDED, 0);

	m_system->set3DSettings(1.f, 500, 1.f);

	m_listenerPosition = new FMOD_VECTOR;
	m_upPosition = new FMOD_VECTOR;
	m_forwardPosition = new FMOD_VECTOR;
	
}

void SoundManager::Update(FreeCamera* _camera)
{
	m_system->update();

	m_listenerPosition->x = _camera->GetTransform()[3].x;
	m_listenerPosition->y = _camera->GetTransform()[3].y;
	m_listenerPosition->z = _camera->GetTransform()[3].z;

	m_upPosition->x = _camera->GetTransform()[1].x;
	m_upPosition->y = _camera->GetTransform()[1].y;
	m_upPosition->z = _camera->GetTransform()[1].z;

	m_forwardPosition->x = -_camera->GetTransform()[2].x;
	m_forwardPosition->y = -_camera->GetTransform()[2].y;
	m_forwardPosition->z = -_camera->GetTransform()[2].z;

	m_system->set3DListenerAttributes(0, m_listenerPosition, 0, m_forwardPosition, m_upPosition);
}

void SoundManager::AddSound(char* _filePath, char* _name)
{
	m_soundList2D.push_back(new Sound(m_system, _filePath));
	m_soundList2D.back()->SetName(_name);
}

void SoundManager::RemoveSound(char* _name)
{
	for (unsigned int i = 0; i < m_soundList2D.size(); i++)
	{
		if (m_soundList2D[i]->GetName() == _name)
		{
			m_soundList2D.erase(m_soundList2D.begin() + i);
		}
	}
}

void SoundManager::RemoveSound(unsigned int _soundID)
{
	m_soundList2D.erase(m_soundList2D.begin() + _soundID);
}

void SoundManager::PlaySound(unsigned int _id)
{
	m_soundList2D[_id]->PlaySound(m_system);
}

void SoundManager::PlaySound(char* _soundName)
{
	for (unsigned int i = 0; i < m_soundList2D.size(); i++)
	{
		if (m_soundList2D[i]->GetName() == _soundName)
		{
			m_soundList2D[i]->PlaySound(m_system);
		}
	}
}

void SoundManager::StopSound(unsigned int _id)
{
	m_soundList2D[_id]->StopSound(m_system);
}

void SoundManager::StopSound(char* _soundName)
{
	for (unsigned int i = 0; i < m_soundList2D.size(); i++)
	{
		if (m_soundList2D[i]->GetName() == _soundName)
		{
			m_soundList2D[i]->StopSound(m_system);
		}
	}
}

void SoundManager::AddSound3D(char* _filePath, char* _name)
{
	m_soundList3D.push_back(new Sound3D(m_system, _filePath, 500));
	m_soundList3D.back()->SetName(_name);
}

void SoundManager::RemoveSound3D(unsigned int _id)
{
	m_soundList3D.erase(m_soundList3D.begin() + _id);
}

void SoundManager::RemoveSound3D(char* _name)
{
	for (unsigned int i = 0; i < m_soundList2D.size(); i++)
	{
		if (m_soundList2D[i]->GetName() == _name)
		{
			m_soundList2D.erase(m_soundList2D.begin() + i);
		}
	}
}

void SoundManager::PlaySound3D(unsigned int _id)
{
	m_soundList3D[_id]->PlaySound(m_system);
}

void SoundManager::PlaySound3D(char* _soundName)
{
	for (unsigned int i = 0; i < m_soundList3D.size(); i++)
	{
		if (m_soundList3D[i]->GetName() == _soundName)
		{
			m_soundList3D[i]->PlaySound(m_system);
		}
	}
}

void SoundManager::StopSound3D(unsigned int _id)
{
	m_soundList3D[_id]->StopSound(m_system);
}

void SoundManager::StopSound3D(char* _soundName)
{
	for (unsigned int i = 0; i < m_soundList3D.size(); i++)
	{
		if (m_soundList3D[i]->GetName() == _soundName)
		{
			m_soundList3D[i]->StopSound(m_system);
		}
	}
}