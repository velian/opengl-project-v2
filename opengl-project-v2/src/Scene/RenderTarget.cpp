#include <GL/gl_core_4_4.h>
#include <GLFW/glfw3.h>
#include <GLM/glm.hpp>
#include <GLM/ext.hpp>

#include "Scene/RenderTarget.h"
#include "Scene/Texture.h"
#include "Scene/Camera.h"

RenderTarget::RenderTarget(POST_TYPE _postType, glm::vec3 _position, glm::vec3 _dimensions)
{
	Initialize(_postType, _position, _dimensions);
}

void RenderTarget::Initialize()
{
	m_shader = new Shader();
	m_shaderData = new ShaderData();

	Create();
}

void RenderTarget::Initialize(POST_TYPE _postType, glm::vec3 _position, glm::vec3 _dimensions)
{
	m_postType = _postType;

	SetDimensions(_dimensions);
	SetPosition(_position);

	Initialize();
}

void RenderTarget::SetPostProcess(POST_TYPE _postType)
{
	m_postType = _postType;
}

void RenderTarget::SetDimensions(glm::vec3 _dimensions)
{
	m_dimensions = _dimensions;
}

void RenderTarget::SetPosition(glm::vec3 _position)
{
	m_position = _position;
}

void RenderTarget::Create()
{
	switch (m_postType)
	{
		case(POST_TYPE::NONE):
		{
			m_shader->LoadProgram(nullptr, "./shader/renderTarget.vs", "./shader/renderTarget.fs");
		} break;

		case(POST_TYPE::BOX_BLUR) :
		{
			m_shader->LoadProgram(nullptr, "./shader/renderTarget.vs", "./shader/renderTargetBoxBlur.fs");
		} break;

		case(POST_TYPE::SHARPEN) :
		{
			m_shader->LoadProgram(nullptr, "./shader/renderTarget.vs", "./shader/renderTargetSharpen.fs");
		} break;

		case(POST_TYPE::DISTORT) :
		{
			m_shader->LoadProgram(nullptr, "./shader/renderTarget.vs", "./shader/renderTargetDistort.fs");
		} break;

		case(POST_TYPE::PATCH) :
		{
			m_shader->LoadProgram(nullptr, "./shader/renderTarget.vs", "./shader/renderTargetPatch.fs");
		} break;
	}	

	float vertexData[] = 
	{
		m_position.x + -m_dimensions.x, -m_dimensions.y, m_position.z + -m_dimensions.z, 1, 0, 0,
		m_position.x +  m_dimensions.x, -m_dimensions.y, m_position.z + -m_dimensions.z, 1, 1, 0,
		m_position.x +  m_dimensions.x,  m_dimensions.y, m_position.z + -m_dimensions.z, 1, 1, 1,
		m_position.x + -m_dimensions.x,  m_dimensions.y, m_position.z + -m_dimensions.z, 1, 0, 1,
	};

	unsigned int indexData[] =
	{
		0, 1, 2,
		0, 2, 3,
	};

	//Setup Frame Buffer
	glGenFramebuffers(1, &m_FBO);
	glBindFramebuffer(GL_FRAMEBUFFER, m_FBO);

	//Texture shtuff
	glGenTextures(1, &m_fboTexture);
	glBindTexture(GL_TEXTURE_2D, m_fboTexture);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGB8, 1280, 720);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_fboTexture, 0);

	glGenRenderbuffers(1, &m_RBO);
	glBindRenderbuffer(GL_RENDERBUFFER, m_RBO);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 1280, 720);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_RBO);

	GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, drawBuffers);

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE)
		printf("Framebuffer Error!\n");
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glGenVertexArrays(1, &m_shaderData->m_VAO);
	glBindVertexArray(m_shaderData->m_VAO);

	glGenBuffers(1, &m_shaderData->m_VBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_shaderData->m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)* 6 * 4, vertexData, GL_STATIC_DRAW);

	glGenBuffers(1, &m_shaderData->m_IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_shaderData->m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * 6, indexData, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(float)* 6, 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float)* 6, ((char*)0) + 16);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}

void RenderTarget::Update(FreeCamera* _camera)
{
	
}

void RenderTarget::Draw(FreeCamera* _camera)
{
	glClearColor(0.25f, 0.25f, 0.25f, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(m_shader->m_programID);

	if (m_fboTexture != -1)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_fboTexture);
		glUniform1i(glGetUniformLocation(m_shader->m_programID, "diffuse"), 0);
	}

	glBindVertexArray(m_shaderData->m_VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
	glBindVertexArray(0);
	glUseProgram(0);
}

void RenderTarget::SetAsActiveFrameBuffer()
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_FBO);
	glViewport(0, 0, 1280, 720);
}

void RenderTarget::ClearAsActiveFrameBuffer()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, 1280, 720);
}