#include <GL/gl_core_4_4.h>
#include <GLFW/glfw3.h>

#include <GLM/glm.hpp>
#include <GLM/ext.hpp>

#include "Scene/GPUParticleEmitter.h"
#include "Scene/Shader.h"
#include "Scene/Camera.h"
#include "GameState/Gamestate.h"

GPUParticleEmitter::GPUParticleEmitter(unsigned int _maxParticles, vec3 _position, Texture* _texture,
	vec4 _startColour, vec4 _endColour,
	float _emitRate, float _startSize, float _endSize)
{
	GPUParticle* _particles = new GPUParticle();
	Initialize(_maxParticles, _particles, _position, _texture, _startColour, _endColour, _emitRate, _startSize, _endSize);
}

void GPUParticleEmitter::Initialize(unsigned int _maxParticles, GPUParticle* _particles, vec3 _position, Texture* _texture,
	vec4 _startColour, vec4 _endColour,
	float _emitRate, float _startSize, float _endSize)
{
	m_particles = _particles;
	m_position = _position;
	m_startColour = _startColour;		
	m_endColour = _endColour;
	m_startSize = _startSize;
	m_endSize = _endSize;
	m_emitRate = _emitRate;
	m_lifeSpanMin = 1;
	m_lifeSpanMax = 2;
	m_maxParticles = _maxParticles;
	m_velocityMin = 0.1f;
	m_velocityMax = 0.25f;
	m_texture = _texture;

	m_updateShader = new Shader();
	m_drawShader = new Shader();

	m_particles = new GPUParticle[m_maxParticles];

	m_activeBuffer = 0;

	Create();
	CreateDrawShader();
	CreateUpdateShader();
}

void GPUParticleEmitter::Create()
{
	m_shaderData = new ShaderData[2];
	
	//Generate
	glGenVertexArrays(1, &(m_shaderData[0].m_VAO));
	glGenBuffers(1, &(m_shaderData[0].m_VBO));
	glGenVertexArrays(1, &(m_shaderData[1].m_VAO));
	glGenBuffers(1, &(m_shaderData[1].m_VBO));
	
	//Setup The Buffers
	glBindVertexArray(m_shaderData[0].m_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, m_shaderData[0].m_VBO);
	glBufferData(GL_ARRAY_BUFFER, m_maxParticles * sizeof(GPUParticle), m_particles, GL_STREAM_DRAW);
	
	glEnableVertexAttribArray(0); //Position
	glEnableVertexAttribArray(1); //Velocity
	glEnableVertexAttribArray(2); //Lifetime
	glEnableVertexAttribArray(3); //Lifespan
	
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 12);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 24);
	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 28);
	
	glBindVertexArray(m_shaderData[1].m_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, m_shaderData[1].m_VBO);
	glBufferData(GL_ARRAY_BUFFER, m_maxParticles * sizeof(GPUParticle), 0, GL_STREAM_DRAW);
	
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 12);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 24);
	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 28);
	
	glBindVertexArray(0);
}

void GPUParticleEmitter::CreateDrawShader()
{
	m_drawShader->LoadProgram(nullptr, "./shader/gpuparticle.vs", "./shader/gpuparticle.fs", "./shader/gpuparticle.gs");
	
	glUseProgram(m_drawShader->m_programID);
	
	//Bind size info
	int location = glGetUniformLocation(m_drawShader->m_programID, "StartSize");
	glUniform1f(location, m_startSize);
	location = glGetUniformLocation(m_drawShader->m_programID, "EndSize");
	glUniform1f(location, m_endSize);
	
	//Bind colour info
	location = glGetUniformLocation(m_drawShader->m_programID, "StartColour");
	glUniform4fv(location, 1, glm::value_ptr(m_startColour));
	location = glGetUniformLocation(m_drawShader->m_programID, "EndColour");
	glUniform4fv(location, 1, glm::value_ptr(m_endColour));
	
	glUseProgram(0);
}

void GPUParticleEmitter::CreateUpdateShader()
{
	char* varyings[] =
	{
		"vPosition", "vVelocity", "vLifetime", "vLifespan"
	};
	
	m_updateShader->LoadProgram(varyings, "./shader/gpuParticleUpdateThruster.vs");
	
	glUseProgram(m_updateShader->m_programID);
	
	int location = glGetUniformLocation(m_updateShader->m_programID, "lifeMin");
	glUniform1f(location, m_lifeSpanMin);
	location = glGetUniformLocation(m_updateShader->m_programID, "lifeMax");
	glUniform1f(location, m_lifeSpanMax);
	
	glUseProgram(0);
}

void GPUParticleEmitter::Update(float _deltaTime)
{
	m_texture->Update(_deltaTime);

	glUseProgram(m_updateShader->m_programID);

	int location = glGetUniformLocation(m_updateShader->m_programID, "time");
	glUniform1f(location, (float)glfwGetTime());

	location = glGetUniformLocation(m_updateShader->m_programID, "deltaTime");
	glUniform1f(location, _deltaTime);

	location = glGetUniformLocation(m_updateShader->m_programID, "emitterPosition");
	glUniform3fv(location, 1, &m_position[0]);

	location = glGetUniformLocation(m_updateShader->m_programID, "lifeMin");
	glUniform1f(location, m_lifeSpanMin);

	location = glGetUniformLocation(m_updateShader->m_programID, "lifeMax");
	glUniform1f(location, m_lifeSpanMax);

	glEnable(GL_RASTERIZER_DISCARD);

	glBindVertexArray(m_shaderData[m_activeBuffer].m_VAO);

	unsigned int secondaryBuffer = (m_activeBuffer + 1) % 2;

	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, m_shaderData[secondaryBuffer].m_VBO);
	glBeginTransformFeedback(GL_POINTS);

	glDrawArrays(GL_POINTS, 0, m_maxParticles);

	glEndTransformFeedback();
	glDisable(GL_RASTERIZER_DISCARD);
	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);

	glUseProgram(0);
}

void GPUParticleEmitter::Draw(FreeCamera* _camera)
{	
	glUseProgram(m_drawShader->m_programID);
	
	unsigned int secondaryBuffer = (m_activeBuffer + 1) % 2;

	int location = glGetUniformLocation(m_drawShader->m_programID, "ProjectionView");
	glUniformMatrix4fv(location, 1, false, &_camera->GetProjectionView()[0][0]);
	
	location = glGetUniformLocation(m_drawShader->m_programID, "CameraTransform");
	glUniformMatrix4fv(location, 1, false, &_camera->GetTransform()[0][0]);

	location = glGetUniformLocation(m_drawShader->m_programID, "StartSize");
	glUniform1f(location, m_startSize);
	location = glGetUniformLocation(m_drawShader->m_programID, "EndSize");
	glUniform1f(location, m_endSize);

	//Bind colour info
	location = glGetUniformLocation(m_drawShader->m_programID, "StartColour");
	glUniform4fv(location, 1, glm::value_ptr(m_startColour));

	location = glGetUniformLocation(m_drawShader->m_programID, "EndColour");
	glUniform4fv(location, 1, glm::value_ptr(m_endColour));

	if (m_texture != nullptr)
	{
		float frameWidthU = (float)m_texture->m_frameWidth / (float)m_texture->m_imageWidth;
		float frameHeightV = (float)m_texture->m_frameHeight / (float)m_texture->m_imageHeight;

		location = glGetUniformLocation(m_drawShader->m_programID, "CurrentFrame");
		glUniform1i(location, m_texture->m_currentFrame);

		location = glGetUniformLocation(m_drawShader->m_programID, "FrameWidth");
		glUniform1f(location, frameWidthU);

		location = glGetUniformLocation(m_drawShader->m_programID, "FrameHeight");
		glUniform1f(location, frameHeightV);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_texture->GetID());

		location = glGetUniformLocation(m_drawShader->m_programID, "tex");
		glUniform1i(location, 0);
	}
	
	glBindVertexArray(m_shaderData[secondaryBuffer].m_VAO);
	glDrawArrays(GL_POINTS, 0, m_maxParticles);
	glBindVertexArray(0);
	
	m_activeBuffer = secondaryBuffer;
	
	glUseProgram(0);
}

void GPUParticleEmitter::HotloadShader()
{
	char* varyings[] =
	{
		"vPosition", "vVelocity", "vLifetime", "vLifespan"
	};

	m_updateShader->LoadProgram(varyings, "./shader/gpuParticleUpdateThruster.vs");

	glUseProgram(m_updateShader->m_programID);

	int location = glGetUniformLocation(m_updateShader->m_programID, "lifeMin");
	glUniform1f(location, m_lifeSpanMin);
	location = glGetUniformLocation(m_updateShader->m_programID, "lifeMax");
	glUniform1f(location, m_lifeSpanMax);

	glUseProgram(0);
}