#include "Scene/Camera.h"
#include <iostream>
#include <GL/gl_core_4_4.h>
#include <GLFW/glfw3.h>

#include "glm\gtc\matrix_inverse.hpp"
#include "glm\gtc\matrix_transform.hpp"
#include "glm\gtx\rotate_vector.hpp"

//---CAMERA---//

Camera::Camera()
{
	
}

Camera::Camera(glm::vec3 _position)
{
	m_worldTransform[3] = glm::vec4(_position, 1);
	m_bPerspectiveSet = false;
}

void Camera::SetTransform(glm::mat4 _transform)
{
	m_worldTransform = _transform;
	UpdateProjectionViewTransform();
}

const glm::mat4 Camera::GetTransform() const
{
	return m_worldTransform;
}

void Camera::SetPosition(glm::vec3 position)
{
	m_worldTransform[3] = glm::vec4(position, 1);
	UpdateProjectionViewTransform();
}

void Camera::LookAt(glm::vec3 lookAt, glm::vec3 _up)
{
	glm::vec4 vLocation = m_worldTransform[3];
	m_worldTransform = glm::inverse(glm::lookAt(vLocation.xyz(), lookAt, _up));
	UpdateProjectionViewTransform();
}

void Camera::LookAt(glm::vec3 position, glm::vec3 lookAt, glm::vec3 _up)
{
	m_worldTransform = glm::inverse(glm::lookAt(position, lookAt, _up));
	UpdateProjectionViewTransform();
}

void Camera::SetupPerspective(float fieldOfView, float aspectRatio)
{
	m_projectionTransform = glm::perspective(fieldOfView, aspectRatio, 0.1f, 10000.0f);
	m_bPerspectiveSet = true;

	UpdateProjectionViewTransform();
}

void Camera::UpdateProjectionViewTransform()
{
	m_viewTransform = glm::inverse(m_worldTransform);
	m_projectionViewTransform = m_projectionTransform * m_viewTransform;
}


//---FREE CAMERA---//

void FreeCamera::Update(double dt)
{
	HandleKeyboardInput(dt);
	HandleMouseInput(dt);
}

void FreeCamera::HandleKeyboardInput(double dt)
{
	//Get the cameras forward/up/right
	glm::mat4 trans = GetTransform();
	glm::vec3 vRight = trans[0].xyz;
	glm::vec3 vUp = trans[1].xyz;
	glm::vec3 vForward = trans[2].xyz;
	glm::vec3 moveDir(0.0f);

	if (glfwGetKey(m_pWindow, GLFW_KEY_W) == GLFW_PRESS)
	{
		moveDir -= vForward;
	}

	if (glfwGetKey(m_pWindow, GLFW_KEY_S) == GLFW_PRESS)
	{
		moveDir += vForward;
	}

	if (glfwGetKey(m_pWindow, GLFW_KEY_A) == GLFW_PRESS)
	{
		moveDir -= vRight;
	}

	if (glfwGetKey(m_pWindow, GLFW_KEY_D) == GLFW_PRESS)
	{
		moveDir += vRight;
	}

	if (glfwGetKey(m_pWindow, GLFW_KEY_SPACE) == GLFW_PRESS)
	{
		moveDir += glm::vec3(0.0f, 1.0f, 0.0f);
	}

	if (glfwGetKey(m_pWindow, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
	{
		SetFlySpeed(150);
	}
	else
	{
		SetFlySpeed(10);
	}

	if (glfwGetKey(m_pWindow, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
	{
		//moveDir -= glm::vec3(0.0f, 1.0f, 0.0f);
	}

	float fLength = glm::length(moveDir);
	if (fLength > 0.01f)
	{
		moveDir = ((float)dt * m_fFlySpeed) * glm::normalize(moveDir);
		SetPosition(GetPosition() + moveDir);
	}
}

void FreeCamera::HandleMouseInput(double dt)
{
	if (glfwGetMouseButton(m_pWindow, GLFW_MOUSE_BUTTON_2) == GLFW_PRESS)
	{
		if (m_bViewButtonClicked == false)
		{
			int width, height;
			glfwGetFramebufferSize(m_pWindow, &width, &height);

			m_dCursorX = width / 2.0;
			m_dCursorY = height / 2.0;

			glfwSetCursorPos(m_pWindow, width / 2, height / 2);

			m_bViewButtonClicked = true;
		}
		else
		{
			double d = 1 - exp(log(0.5)*50*dt);
			double mouseX, mouseY;
			glfwGetCursorPos(m_pWindow, &mouseX, &mouseY);

			double xOffset = (mouseX - m_dCursorX)*d;
			double yOffset = (mouseY - m_dCursorY)*d;

			CalculateRotation(dt, xOffset, yOffset);
			glfwSetInputMode(m_pWindow, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

		}

		int width, height;
		glfwGetFramebufferSize(m_pWindow, &width, &height);
		glfwSetCursorPos(m_pWindow, width / 2, height / 2);
	}
	else
	{
		glfwSetInputMode(m_pWindow, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		m_bViewButtonClicked = false;
	}
}

void FreeCamera::CalculateRotation(double dt, double xOffset, double yOffset)
{
	if (xOffset != 0.0)
	{
		glm::mat4 rot = glm::rotate((float)(m_fRotationSpeed * dt * -xOffset), glm::vec3(0, 1, 0));

		SetTransform(GetTransform() * rot);
	}

	if (yOffset != 0.0)
	{
		glm::mat4 rot = glm::rotate((float)(m_fRotationSpeed * dt * -yOffset), glm::vec3(1, 0, 0));

		SetTransform(GetTransform() * rot);
	}

	//Clean up rotation
	glm::mat4 oldTrans = GetTransform();

	glm::mat4 trans;

	glm::vec3 worldUp = glm::vec3(0, 1, 0);

	//Right
	trans[0] = glm::normalize(glm::vec4(glm::cross(worldUp, oldTrans[2].xyz()), 0));
	//Up
	trans[1] = glm::normalize(glm::vec4(glm::cross(oldTrans[2].xyz(), trans[0].xyz()), 0));
	//Forward
	trans[2] = glm::normalize(oldTrans[2]);

	//Position
	trans[3] = oldTrans[3];

	SetTransform(trans);
}