#include "Scene/Shadow.h"
#include "Scene/Shader.h"
#include "Scene/Light.h"
#include "Scene/Camera.h"
#include "TINYOBJ/Object.h"
#include "TINYOBJ/ObjectData.h"

#include <GL/gl_core_4_4.h>
#include <GLFW/glfw3.h>

Shadow::Shadow()
{

}

void Shadow::Initialize()
{
	m_shaderData = new ShaderData();
	m_shadowGenShader = new Shader();

	Create();
}

void Shadow::Create()
{
	m_shadowGenShader->LoadProgram(nullptr, "./shader/shadowGen.vs", "./shader/shadowGen.fs");

	glGenFramebuffers(1, &m_FBO);
	glBindFramebuffer(GL_FRAMEBUFFER, m_FBO);

	glGenTextures(1, &m_FBODepth);
	glBindTexture(GL_TEXTURE_2D, m_FBODepth);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, 1024, 1024, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	// attached as a depth attachment to capture depth not colour
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, m_FBODepth, 0);

	// no colour targets are used
	glDrawBuffer(GL_NONE);

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

	if (status != GL_FRAMEBUFFER_COMPLETE)
	{
		printf("Framebuffer Error!\n");
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Shadow::Update()
{

}

void Shadow::Draw(FreeCamera* _camera, Object* _gameObject)
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_FBO);
	glViewport(0, 0, 1024, 1024);
	glClear(GL_DEPTH_BUFFER_BIT);

	glUseProgram(m_shadowGenShader->m_programID);

	int location = glGetUniformLocation(m_shadowGenShader->m_programID, "LightMatrix");
	glUniformMatrix4fv(location, 1, GL_FALSE, &(_gameObject->GetLight()->m_lightMatrix[0][0]));

	for (unsigned int i = 0; i < _gameObject->GetData()->m_objectShapeSize; ++i)
	{
		glBindVertexArray(_gameObject->GetShaderData()[i].m_VAO);
		glDrawElements(GL_TRIANGLES, _gameObject->GetData()->m_indexCount, GL_UNSIGNED_INT, 0);
	}

	glUseProgram(0);
}