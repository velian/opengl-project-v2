#include <GL/gl_core_4_4.h>
#include <GLFW/glfw3.h>
#include "Scene/Shader.h"
#include "TINYOBJ/Object.h"
#include "TINYOBJ/ObjectData.h"
#include "Scene/Texture.h"
#include "Scene/Camera.h"
#include "GameState/GameState.h"
#include "Scene/Light.h"
#include "FBX/FBXObject.h"

Shader::Shader()
{
	Initialize();
}

void Shader::Initialize()
{
	m_programID = 0;
}

void Shader::LoadProgram(char** _varyings, char* _vsPath, char* _fsPath, char* _gsPath)
{
	unsigned int vertexShaderId   = _vsPath ? LoadShader(_vsPath, GL_VERTEX_SHADER) : NULL;
	unsigned int fragmentShaderId = _fsPath ? LoadShader(_fsPath, GL_FRAGMENT_SHADER) : NULL;
	unsigned int geometryShaderId = _gsPath ? LoadShader(_gsPath, GL_GEOMETRY_SHADER) : NULL;

	unsigned int programID = CreateProgram(_varyings, vertexShaderId, fragmentShaderId, geometryShaderId);

	glDeleteShader(vertexShaderId);
	glDeleteShader(fragmentShaderId);
	glDeleteShader(geometryShaderId);

	m_programID = programID;
}

unsigned int Shader::LoadShader(char* _filePath, unsigned int _type)
{
	int success = GL_FALSE;

	unsigned int handle = glCreateShader(_type);
	unsigned char* source = FileToBuffer(_filePath);

	glShaderSource(handle, 1, (const char**)&source, 0);
	glCompileShader(handle);

	glGetShaderiv(handle, GL_COMPILE_STATUS, &success);

	if (success == GL_FALSE)
	{
		int infoLength = 0;
		glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &infoLength);
		char* infoLog = new char[infoLength];

		glGetShaderInfoLog(handle, infoLength, 0, infoLog);
		printf("Faliled to compile shader program : %s", infoLog);
		delete[] infoLog;

		return 0;
	}

	delete[] source;

	return handle;
}

unsigned char* Shader::FileToBuffer(char* _filePath)
{
	FILE* file = fopen(_filePath, "rb");

	if (file == nullptr)
	{
		printf("Couldn't open file : %s", _filePath);
		return nullptr;
	}

	// get number of bytes in file
	fseek(file, 0, SEEK_END);
	unsigned int uiLength = (unsigned int)ftell(file);
	fseek(file, 0, SEEK_SET);

	// allocate buffer and read file contents
	unsigned char* acBuffer = new unsigned char[uiLength + 1];
	memset(acBuffer, 0, uiLength + 1);
	fread(acBuffer, sizeof(unsigned char), uiLength, file);

	fclose(file);
	return acBuffer;
}

unsigned int Shader::CreateProgram(char** _varyings, GLuint _vs, GLuint _fs, GLuint _gs)
{
	int success = GL_FALSE;

	unsigned int handle = glCreateProgram();
	if (_vs != NULL) glAttachShader(handle, _vs);
	if (_fs != NULL) glAttachShader(handle, _fs);
	if (_gs != NULL) glAttachShader(handle, _gs);

	if (_varyings != nullptr)
	{
		glTransformFeedbackVaryings(handle, 4, _varyings, GL_INTERLEAVED_ATTRIBS);
	}

	glLinkProgram(handle);
	glGetProgramiv(handle, GL_LINK_STATUS, &success);

	if (success == GL_FALSE)
	{
		int infoLength = 0;
		glGetProgramiv(handle, GL_INFO_LOG_LENGTH, &infoLength);
		char* infoLog = new char[infoLength];

		glGetProgramInfoLog(handle, infoLength, 0, infoLog);
		printf("Faliled to link shader program : %s", infoLog);
		delete[] infoLog;

		return 0;
	}

	return handle;
}

void Shader::OpenDialogue(SHADER_TYPE _shaderType, SHADER_TYPE _secondaryShader)
{
	OPENFILENAME firstShader;
	OPENFILENAME secondShader;
	char szFileName[MAX_PATH] = "";

	ZeroMemory(&firstShader, sizeof(firstShader));
	ZeroMemory(&secondShader, sizeof(secondShader));

	firstShader.lStructSize = sizeof(firstShader);
	secondShader.lStructSize = sizeof(secondShader);

	switch (_shaderType)
	{
		case (SHADER_TYPE::VERTEX) :
		{
			firstShader.lpstrFilter = "Vertex Shader (*.vs)\0*.vs";
			break;
		}
		case (SHADER_TYPE::FRAGMENT) :
		{
			firstShader.lpstrFilter = "Fragment Shader (*.fs)\0*.fs";
			break;
		}
	}

	switch (_secondaryShader)
	{
		case (SHADER_TYPE::VERTEX) :
		{
			secondShader.lpstrFilter = "Vertex Shader (*.vs)\0*.vs";
			break;
		}
		case (SHADER_TYPE::FRAGMENT) :
		{
			secondShader.lpstrFilter = "Fragment Shader (*.fs)\0*.fs";
			break;
		}
	}

	firstShader.lpstrFile = szFileName;
	firstShader.nMaxFile = MAX_PATH;
	firstShader.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;

	secondShader.lpstrFile = szFileName;
	secondShader.nMaxFile = MAX_PATH;
	secondShader.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
	//ofn.lpstrDefExt = L"txt";

	std::string vertexPath;
	std::string fragmentPath;

	if (GetOpenFileName(&firstShader))
	{
		vertexPath = firstShader.lpstrFile;

		printf("Loaded Shader From File: %s", vertexPath.c_str());
	}

	if (GetOpenFileName(&secondShader))
	{
		fragmentPath = secondShader.lpstrFile;

		printf("Loaded Shader From File: %s", fragmentPath.c_str());
	}
	else
		return;

	LoadProgram(nullptr, (char*)vertexPath.c_str(), (char*)fragmentPath.c_str());
}