#include "Scene/Sound3D.h"

Sound3D::Sound3D()
{
	m_sound = nullptr;
}

Sound3D::Sound3D(FMOD::System* _system, char* _filePath, unsigned int _distanceFactor)
{
	Initialize(_system, _filePath, _distanceFactor);
}

void Sound3D::SetPosition(glm::vec3 _position, FMOD::System* _system)
{
	m_position.x = _position.x;
	m_position.y = _position.y;
	m_position.z = _position.z;
	_system->set3DListenerAttributes(0, &m_position, 0, 0, 0);
}

void Sound3D::Initialize(FMOD::System* _system, char* _filePath, unsigned int _distanceFactor)
{
	_system->createSound(_filePath, FMOD_3D, 0, &m_sound);
	m_sound->set3DMinMaxDistance(0.1f * (float)_distanceFactor, (float)_distanceFactor);
	m_sound->setMode(FMOD_LOOP_NORMAL);
}

void Sound3D::PlaySound(FMOD::System* _system)
{
	_system->playSound(m_sound, 0, true, &m_channel);
	m_channel->set3DAttributes(&m_position, 0);
	m_channel->setPaused(false);
}

void Sound3D::StopSound(FMOD::System* _system)
{
	_system->playSound(m_sound, 0, true, 0);
}

void Sound3D::SetName(char* _name)
{
	m_name = _name;
}