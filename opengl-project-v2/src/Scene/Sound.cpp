#include "Scene/Sound.h"

Sound::Sound()
{
	m_sound = nullptr;
}

Sound::Sound(FMOD::System* _system, char* _filePath)
{
	Initialize(_system, _filePath);
}

void Sound::Initialize(FMOD::System* _system, char* _filePath)
{
	_system->createSound(_filePath, FMOD_3D, 0, &m_sound);
}

void Sound::PlaySound(FMOD::System* _system)
{
	_system->playSound(m_sound, 0, false, 0);
}

void Sound::StopSound(FMOD::System* _system)
{
	_system->playSound(m_sound, 0, true, 0);
}

void Sound::SetName(char* _name)
{
	m_name = _name;
}