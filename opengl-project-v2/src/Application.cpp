#define GLFW_INCLUDE_GLCOREARB
#define NANOVG_GL3_IMPLEMENTATION

#include <GL/gl_core_4_4.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <conio.h>

#include <IMGUI/imgui.h>
#include <IMGUI/imgui_impl_glfw_gl3.h>

#include "Application.h"
#include "GameState/GameStateManager.h"

Application::Application(int _screenWidth, int _screenHeight, char* _screenTitle){
	Initialize(_screenWidth, _screenHeight, _screenTitle);
}

Application::~Application(){
	delete m_gameStateManager;
}

int Application::Initialize(int _screenWidth, int _screenHeight, char* _screenTitle){
	m_running = true;
	m_deltaTime = 0.f;
	m_previousTime = 0.f;
	m_currentTime = 0.f;

	if (glfwInit() == false){
		std::cout << "GLFW failed to initialize!";
		return -1;
	}

	m_window = glfwCreateWindow(_screenWidth, _screenHeight, _screenTitle, nullptr, nullptr);

	if (m_window == nullptr){
		glfwTerminate();
		return -2;
	}

	glfwMakeContextCurrent(m_window);

	if (ogl_LoadFunctions() == ogl_LOAD_FAILED){ 
		glfwDestroyWindow(m_window);
		glfwTerminate();
		return -3;
	}

	m_gameStateManager = new GameStateManager();

	ImGui_ImplGlfwGL3_Init(m_window, true);

	return 1;
}

void scroll(GLFWwindow* window, double x, double y){
	printf("%d, %d\n", x, y);
}

void Application::Run(){	
	
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	

	m_gameStateManager->SetContext(m_window);
	
	while (glfwWindowShouldClose(m_window) == false && m_running == true){
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	
		glClearColor(0.f, 0.f, 0.f, 1.f);
		glfwPollEvents();

		UpdateDelta();

		glEnable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);

		ImGui_ImplGlfwGL3_NewFrame();

		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		ImGui::Separator();

		m_gameStateManager->Update(m_deltaTime);
		m_gameStateManager->Draw();		

		ImGui::Render();

		glfwSwapBuffers(m_window);		

		if (glfwGetKey(m_window, GLFW_KEY_ESCAPE)){
			m_running = false;
		}	
	}
}

void Application::UpdateDelta(){
	m_currentTime = (float)glfwGetTime();

	m_deltaTime = m_currentTime - m_previousTime;
	m_previousTime = m_currentTime;
}

void Application::Shutdown(){	
	glfwDestroyWindow(m_window);
	glfwTerminate();
	delete this;
}