#ifndef GAME_STATE_MANAGER_H
#define GAME_STATE_MANAGER_H

#include <vector>
#include "GameState/States/SplashState.h"
#include "GameState/States/TitleState.h"
#include "GameState/States/TestState.h"

struct GLFWwindow;

class GameState;
class GameStateManager
{
public:

	GameStateManager(){}
	~GameStateManager();

	void Update(float _deltaTime);
	void Draw();

	void SetContext(GLFWwindow* _window);

	void AddState(GameState* _state);
	void SetState(unsigned int _state);

	void NextState();
	void PreviousState();

	unsigned int GetState();
	unsigned int GetStateSize();

protected:

	std::vector<GameState*> m_stateList;

	unsigned int m_currentState;
	unsigned int m_previousState;

	GLFWwindow* m_window;

	//List of states
	SplashState* m_splashState;
	TitleState* m_titleState;
	TestState* m_testState;
};

#endif