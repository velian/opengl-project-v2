#ifndef TEST_STATE_H
#define TEST_STATE_H

#include "GameState/GameState.h"

class Object;
class FBXObject;
class FreeCamera;
class RenderTarget;
class Skybox;
class Shadow;
class TestState : public GameState
{
public:

	TestState(GLFWwindow* _window, GameStateManager* _manager, unsigned int _id);

	virtual void Update(float _deltaTime);
	virtual void Draw();

	virtual void DrawGUI(char* _stateName);

private:

	bool m_toggle;

	FreeCamera* m_camera;
	Object* m_object;
	FBXObject* m_fbxObject;
	RenderTarget* m_renderTarget;
	Skybox* m_skybox;
	Shadow* m_shadow;
};

#endif