#ifndef TITLE_STATE_H
#define TITLE_STATE_H

#include "GameState/GameState.h"

class TitleState : public GameState
{
public:

	TitleState(GLFWwindow* _window, GameStateManager* _manager, unsigned int _id);

	virtual void Update(float _deltaTime);
	virtual void Draw();
};

#endif