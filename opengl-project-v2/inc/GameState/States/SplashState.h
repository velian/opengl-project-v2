#ifndef SPLASH_STATE_H
#define SPLASH_STATE_H

#include "GameState/GameState.h"

class SplashState : public GameState
{
public:

	SplashState(GLFWwindow* _window, GameStateManager* _manager, unsigned int _id);

	virtual void Update(float _deltaTime);
	virtual void Draw();
};

#endif