#ifndef GAME_STATE_H
#define GAME_STATE_H

struct GLFWwindow;

class GameStateManager;
class GameState
{
public:

	GameState(GLFWwindow* _window, GameStateManager* _manager, unsigned int _id);

	virtual void Update(float _deltaTime);
	virtual void Draw();	

	unsigned int m_id;

protected:

	virtual void DrawGUI(char* _stateName = "DEBUG STATE");

	GameStateManager* m_manager;
	GLFWwindow* m_window;
};

#endif