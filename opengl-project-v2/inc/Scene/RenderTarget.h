#ifndef RENDERTARGET_H
#define RENDERTARGET_H

#include "Shader.h"
#include <glm/glm.hpp>

class FreeCamera;
class Texture;

enum POST_TYPE
{
	NONE,
	SHARPEN,
	BOX_BLUR,
	DISTORT,
	PATCH
};

class RenderTarget
{
public:

	RenderTarget(POST_TYPE _postType, glm::vec3 _position = glm::vec3(0, 0, 0), glm::vec3 _dimensions = glm::vec3(1, 1, 0));

	void Initialize();
	void Initialize(POST_TYPE _postType, glm::vec3 _position, glm::vec3 _dimensions);

	void SetDimensions(glm::vec3 _dimensions);
	void SetPosition(glm::vec3 _position);

	//Update - Child to camera
	void Update(FreeCamera* _camera);
	void Draw(FreeCamera* _camera);

	Shader* GetShader(){ return m_shader; }

	void SetPostProcess(POST_TYPE _postType);

	void SetAsActiveFrameBuffer();
	void ClearAsActiveFrameBuffer();
private:

	void Create();

	Shader* m_shader;
	ShaderData* m_shaderData;

	glm::vec3 m_dimensions;
	glm::vec3 m_position;

	unsigned int m_FBO;
	unsigned int m_RBO;
	unsigned int m_fboTexture;

	unsigned int m_postType;

};

#endif