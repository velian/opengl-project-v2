#ifndef VERTEX_H
#define VERTEX_H

#include "glm/glm.hpp"

struct Vertex
{
	//Vertex()
	//{
	//	x = 0, y = 0, z = 0, w = 0;
	//	nx = 0, ny = 0, nz = 0, nw = 0;
	//	tx = 0, ty = 0, tz = 0, tw = 0;
	//	s = 0, t = 0;
	//}

	float x, y, z, w;
	float nx, ny, nz, nw;
	float tx, ty, tz, tw;
	float s, t;

	void SetPosition(glm::vec4 _toSet){ x = _toSet.x, y = _toSet.y, z = _toSet.z, w = _toSet.w; }
	void SetNormals(glm::vec4 _toSet) { nx = _toSet.x, ny = _toSet.y, nz = _toSet.z, nw = _toSet.w; }
	void SetTangents(glm::vec4 _toSet) { tx = _toSet.x, ty = _toSet.y, tz = _toSet.z, tw = _toSet.w; }
	void SetTexCoords(glm::vec2 _toSet) { s = _toSet.x, t = _toSet.y; }
};

#endif