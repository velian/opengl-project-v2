#ifndef SHADER_H
#define SHADER_H

#include <vector>
#include <fstream>
#include <string>
#include <iomanip>
#include <Windows.h>
#include <Commdlg.h>
#include <algorithm>
#include <string>

#include "atlstr.h"
#include <GLM/glm.hpp>

enum SHADER_TYPE
{
	VERTEX,
	FRAGMENT,
	GEOMETRY
};

struct ShaderData
{
	ShaderData()
	{
		m_VAO = 0;
		m_VBO = 0;
		m_IBO = 0;
	}

	unsigned int m_VAO;
	unsigned int m_VBO;
	unsigned int m_IBO;
};

struct ObjectData;

class FBXObject;
class FreeCamera;
class Object;
class GameState;
class Shader
{
public:

	Shader();

	void Initialize();

	void LoadProgram(char** _varyings = nullptr, char* _vsPath = nullptr, char* _fsPath = nullptr, char* _gsPath = nullptr);

	void OpenDialogue(SHADER_TYPE _shaderType = SHADER_TYPE::VERTEX, SHADER_TYPE _secondaryShader = SHADER_TYPE::FRAGMENT);

	unsigned int m_programID;

private:

	unsigned int LoadShader(char* _filePath, unsigned int _type);
	unsigned int CreateProgram(char** _varyings = nullptr, unsigned int _vs = NULL, unsigned int _fs = NULL, unsigned int _gs = NULL);
	unsigned char* FileToBuffer(char* _filePath);
};

#endif