#ifndef TEXTURE_H
#define TEXTURE_H

#include <GL/gl_core_4_4.h>
#include <GLFW/glfw3.h>
#include <GLM/glm.hpp>
#include <vector>

#include "Scene/Shader.h"

enum DRAW_TYPE
{
	NEAREST = 0,
	LINEAR = 1
};

enum TEX_TYPE
{
	PNG = 0,
	JPG = 1,
	TGA = 2
};

class Camera;
class Texture
{
public:

	Texture();
	Texture(Shader* _shader){ m_shader = _shader; }

	void Initialize(std::string _filePath, DRAW_TYPE _drawType = LINEAR);
	void Initialize(std::string _filePath, int _frameWidth = 16, int _frameHeight = 16, float _startTime = 1, DRAW_TYPE _drawType = LINEAR);

	void LoadCubeMap(std::vector<const GLchar*> faces);

	virtual void Update(float _deltaTime);
	virtual void Draw(FreeCamera* _camera);

	void OpenDialogue(DRAW_TYPE _drawType = LINEAR);

	unsigned int GetID(){ return m_id; }
	std::string GetCurrentPath() { return m_currentPath; }

	unsigned int m_id;

	int m_currentFrame;
	int m_maxFrames;
	int m_frameWidth, m_frameHeight;
	int m_imageWidth, m_imageHeight;

	glm::vec2 m_uv;

private:

	Shader* m_shader;

	

	std::string m_currentPath;

	bool m_spriteSheet;

	float m_startTime;
	float m_timer;
	
	int m_imageFormat;

	unsigned char* m_data;
	
};

#endif