#ifndef LIGHT_H
#define LIGHT_H

#include "glm\glm.hpp"
#include "glm\ext.hpp"

class Light
{
public:

	Light( glm::vec3 _lightDirection ) : 
		m_ambientLight(1), 
		m_specular(0.25), 
		m_lightColour(glm::vec3(1, 1, 1)),
		m_shadowColour(glm::vec3(0, 0, 0)),
		m_lightDirection(glm::normalize(_lightDirection))
	{
		CalculateProjection();
	}

	void SetLightDirection( glm::vec3 _lightDirection )
	{
		m_lightDirection = _lightDirection;
		CalculateProjection();
	}

	void CalculateProjection()
	{
		if (m_lightDirection != glm::vec3(0, 0, 0))
		{
			m_lightProjection = glm::ortho<float>(-10, 10, -10, 10, -10, 10);
			m_lightView = glm::lookAt(m_lightDirection, glm::vec3(0), glm::vec3(0, 1, 0));

			m_lightMatrix = m_lightProjection * m_lightView;
		}
		else
		{
			printf("Setup Direction Is glm::vec3( 0, 0, 0 )!\nLight Setup Failed");
		}		
	}

	float m_ambientLight;
	float m_specular;

	glm::vec3 m_lightColour;	
	glm::vec3 m_lightDirection;	
	glm::vec3 m_shadowColour;

	glm::mat4 m_lightView;
	glm::mat4 m_lightProjection;
	glm::mat4 m_lightMatrix;
};

#endif