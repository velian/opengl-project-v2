#ifndef SOUND3D_H
#define SOUND3D_H

#include "FMOD\fmod.hpp"
#include "glm\glm.hpp"

class Sound3D
{
public:

	Sound3D();
	Sound3D(FMOD::System* _system, char* _filePath, unsigned int _distanceFactor);

	void Initialize(FMOD::System* _system, char* _filePath, unsigned int _distanceFactor);

	void SetName(char* _name);
	char* GetName() { return m_name; }

	FMOD::Sound* GetSound(){ return m_sound; }

	void PlaySound(FMOD::System* _system);
	void StopSound(FMOD::System* _system);

	glm::vec3 GetPosition(){ return glm::vec3(m_position.x, m_position.y, m_position.z); }
	void SetPosition(glm::vec3 _position, FMOD::System* _system);
									     

private:

	char* m_name;
	FMOD_VECTOR m_position;
	FMOD::Sound* m_sound;
	FMOD::Channel* m_channel;
};

#endif