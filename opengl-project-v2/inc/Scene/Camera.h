////
// Author : James Whyte
// Description : Struct contains data for projection camera
////

#ifndef CAMERA_H
#define CAMERA_H

#include <GLM/glm.hpp>

struct GLFWwindow;

class Camera
{
public:

	Camera();
	Camera(glm::vec3 _position);
	
	void SetTransform(glm::mat4 _transform);
	const glm::mat4 GetTransform() const;

	void SetPosition(glm::vec3 position);
	glm::vec3 GetPosition() const { return m_worldTransform[3].xyz(); }

	void LookAt(glm::vec3 lookAt, glm::vec3 _up);
	void LookAt(glm::vec3 position, glm::vec3 lookAt, glm::vec3 _up);

	void SetupPerspective(float fieldOfView, float aspectRatio);

	const glm::mat4& GetProjection() const { return m_projectionTransform; }
	const glm::mat4& GetView() const { return m_viewTransform; }
	const glm::mat4& GetProjectionView() const { return m_projectionViewTransform; }

	virtual void Update(float _deltaTime){};

	bool GetPerspectiveSet() const { return m_bPerspectiveSet; }

protected:

	void UpdateProjectionViewTransform();

	GLFWwindow* m_window;

	bool m_bPerspectiveSet;
	
	glm::mat4 m_viewTransform;
	glm::mat4 m_projectionTransform;
	glm::mat4 m_projectionViewTransform;

	glm::mat4 m_worldTransform;
};

class FreeCamera : public Camera
{
public:
	FreeCamera() : FreeCamera(10.0f, 5.0f)
	{
		m_bViewButtonClicked = false;
	}
	FreeCamera(float flySpeed, float rotSpeed) : Camera(), m_fFlySpeed(flySpeed), m_fRotationSpeed(rotSpeed)
	{

	}
	~FreeCamera() {};

	virtual void Update(double dt);

	void SetFlySpeed(float fSpeed) { m_fFlySpeed = fSpeed; }
	float GetFlySpeed() const { return m_fFlySpeed; }

	float GetRotationSpeed() const { return m_fRotationSpeed; }
	void SetRotationSpeed(float val) { m_fRotationSpeed = val; }

	//TEMP - Pass the window to be used to query the keyboard
	//This will get changed to an Input Manager later
	void SetInputWindow(GLFWwindow* pWindow) { m_pWindow = pWindow; }


protected:
	void HandleKeyboardInput(double dt);
	void HandleMouseInput(double dt);

	void CalculateRotation(double dt, double xOffset, double yOffset);

	GLFWwindow* m_pWindow;
	float m_fFlySpeed;
	float m_fRotationSpeed;
	bool m_bViewButtonClicked;
	double m_dCursorX, m_dCursorY;

};

#endif