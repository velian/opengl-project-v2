#ifndef SKYBOX_H
#define SKYBOX_H

#include <GL/gl_core_4_4.h>
#include <GLFW/glfw3.h>
#include <vector>

struct ShaderData;

class Texture;
class FreeCamera;
class Shader;
class Skybox
{
public:

	Skybox(std::vector<const GLchar*> _filePaths);

	void Initialize(std::vector<const GLchar*> _filePaths);

	void Draw(FreeCamera* _camera);

private:

	void Create();

	Shader* m_shader;
	ShaderData* m_shaderData;
	Texture* m_texture;
};

#endif