#ifndef GPUPARTICLE_EMITTER_H
#define GPUPARTICLE_EMITTER_H

#include <GLM/glm.hpp>

#include "Scene/Texture.h"
#include "Scene/GPUParticle.h"
#include "Scene/Shader.h"

using namespace glm;
class FreeCamera;
class GPUParticleEmitter
{
public:

	GPUParticleEmitter(unsigned int _maxParticles = 1000, vec3 _position = vec3(0, 0, 0), Texture* _texture = nullptr,
		vec4 _startColour = vec4(1, 1, 1, 1), vec4 _endColour = vec4(1, 1, 1, 1),
		float _emitRate = 25.f, float _startSize = 0.1f, float _endSize = 0.2f);

	virtual void Update(float _deltaTime);
	virtual void Draw(FreeCamera* _camera);

	void InitializeTweak(GameState* _gameState);

	void HotloadShader();

	Shader* GetShader() { return m_updateShader; }

protected:

	void Initialize(unsigned int _maxParticles, GPUParticle* _particles, vec3 _position, Texture* _texture,
		vec4 _startColour, vec4 _endColour,
		float _emitRate, float _startSize, float _endSize);

	void Create();
	void CreateUpdateShader();
	void CreateDrawShader();

	//Variables//
	GPUParticle* m_particles;

	Shader* m_updateShader;
	Shader* m_drawShader;

	ShaderData* m_shaderData;

	Texture* m_texture;

	vec3 m_position;

	vec4 m_startColour;
	vec4 m_endColour;

	float m_emitTimer;
	float m_emitRate;

	float m_lifeSpanMin;
	float m_lifeSpanMax;

	float m_velocityMin;
	float m_velocityMax;

	float m_startSize;
	float m_endSize;

	unsigned int m_firstDead;
	unsigned int m_maxParticles;

	unsigned int m_activeBuffer;

	float m_lastDrawTime;
};

#endif