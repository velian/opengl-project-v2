#ifndef SOUND_H
#define SOUND_H

#include "FMOD\fmod.hpp"

class Sound
{
public:

	Sound();
	Sound(FMOD::System* _system, char* _filePath);

	void Initialize(FMOD::System* _system, char* _filePath);

	void SetName(char* _name);
	char* GetName() { return m_name; }

	void PlaySound(FMOD::System* _system);
	void StopSound(FMOD::System* _system);

private:

	char* m_name;
	FMOD::Sound* m_sound;
};

#endif