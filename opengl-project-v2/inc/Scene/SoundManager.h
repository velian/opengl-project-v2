#ifndef SOUND_MANAGER_H
#define SOUND_MANAGER_H

#include "FMOD\fmod.hpp"
#include <vector>

class Sound;
class Sound3D;
class FreeCamera;

using namespace std;
class SoundManager
{
public:

	SoundManager();

	void Initialize();

	void Update(FreeCamera* _camera);

	void AddSound(char* _filePath, char* _name);
	void AddSound3D(char* _filePath, char* _name);
	
	void RemoveSound(unsigned int _id);
	void RemoveSound(char* _name);

	void RemoveSound3D(unsigned int _id);
	void RemoveSound3D(char* _name);

	void PlaySound(unsigned int _id);
	void PlaySound(char* _soundName);
	void StopSound(unsigned int _id);
	void StopSound(char* _soundName);

	void PlaySound3D(unsigned int _id);
	void PlaySound3D(char* _soundName);
	void StopSound3D(unsigned int _id);
	void StopSound3D(char* _soundName);

private:

	FMOD::System* m_system;
	FMOD::Channel* m_channel;

	FMOD_VECTOR* m_listenerPosition;
	FMOD_VECTOR* m_upPosition;
	FMOD_VECTOR* m_forwardPosition;

	vector<Sound*>	 m_soundList2D;
	vector<Sound3D*> m_soundList3D;
};

#endif