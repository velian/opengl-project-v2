#ifndef SHADOW_H
#define SHADOW_H

class Shader;
struct ShaderData;
class Light;
class FreeCamera;
class Object;

class Shadow
{
public:

	Shadow();

	void Initialize();

	void Update();
	void Draw(FreeCamera* _camera, Object* _gameObject);

	unsigned int m_FBO;
	unsigned int m_FBODepth;

private:

	void Create();

	ShaderData* m_shaderData;
	Shader* m_shadowSecondary;
	Shader* m_shadowGenShader;
};

#endif