#ifndef APPLICATION_H
#define APPLICATION_H

#include "glm/glm.hpp"

struct GLFWwindow;

class NVGcontext;
class GameStateManager;
class DemoData;
class Application
{
public:

	Application(int _screenWidth = 800, int _screenHeight = 600, char* _screenTitle = "GLFW Window");
	~Application();

	void Run();	
	void Shutdown();

private:

	int Initialize(int _screenWidth, int _screenHeight, char* _screenTitle = "GLFW Window");

	void UpdateDelta();	

	GLFWwindow* m_window;
	GameStateManager* m_gameStateManager;
	NVGcontext* gui;
	DemoData* demoData;

	bool m_running;
	float m_deltaTime;
	float m_previousTime;
	float m_currentTime;
};

#endif