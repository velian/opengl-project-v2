#version 410

layout(location = 0) in vec4 Position;
layout(location = 1) in vec4 Indices;
layout(location = 2) in vec4 Weights;
layout(location = 3) in vec4 Normal;
layout(location = 4) in vec2 Texcoord;

out vec4 vPosition;
out vec4 vNormal;
out vec2 vTexcoord;
out vec4 vWeights;

const int MAX_BONES = 128;

uniform mat4 ProjectionView, global;
uniform mat4 Bones[MAX_BONES];

void main()
{
	vNormal = Normal;
	vTexcoord = Texcoord;
	vPosition = Position;

	vec4 P = vec4(0,0,0,0);
	ivec4 I = ivec4(Indices);

	P = Bones[I.x] * Position * Weights.x;
	P += Bones[I.y] * Position * Weights.y;
	P += Bones[I.z] * Position * Weights.z;
	P += Bones[I.w] * Position * Weights.w;
	P.w = 1;

	gl_Position = ProjectionView * global * P;
}