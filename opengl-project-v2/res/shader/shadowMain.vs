#version 410

layout(location=0) in vec3 Position;
layout(location=1) in vec3 Normal;

out vec3 vNormal;

uniform mat4 ProjectionView;

void main()
{
	vNormal = Normal;
	gl_Position = ProjectionView * vec4(Position, 1);
}