#version 410

layout(location=0) in vec3 Position;
layout(location=1) in vec3 Velocity;
layout(location=2) in float Lifetime;
layout(location=3) in float Lifespan;

out vec3 vPosition;
out vec3 vVelocity;
out float vLifetime;
out float vLifespan;

uniform float time;
uniform float deltaTime;
uniform float lifeMin;
uniform float lifeMax;
uniform vec3 emitterPosition;

const float INVERSE_MAX_UINT = 1.0f / 4294967295.0f;

float rand(uint seed, float range)
{
	uint i = (seed ^ 12345391u) * 2654435769u;
	i ^= (i << 6u) ^ (i >> 26u);
	i *= 2654435769u;
	i += (i << 5u) ^ (i >> 12u);
	return float(range * i) * INVERSE_MAX_UINT;
}

void main()
{
	uint seed = uint(time * 1000.0) + uint(gl_VertexID);

	vPosition = Position + Velocity * deltaTime;	
	vVelocity = Velocity;
	vLifetime = Lifetime + deltaTime;
	vLifespan = Lifespan;

	if(vLifetime > vLifespan)
	{
		vVelocity.x = rand(seed++, 2) - 1;
		vVelocity.y = rand(seed++, 2) - 1;
		vVelocity.z = rand(seed++, 2) - 1;

		vVelocity = normalize(vVelocity);

		vVelocity.x += cos(deltaTime) * 0;
		vVelocity.y += cos(deltaTime) * 0;
		vVelocity.z += cos(deltaTime) * 4;

		vPosition = emitterPosition;
		vLifetime = 0;
		vLifespan = rand(seed++, lifeMax - lifeMin) + lifeMin;
	}
}