#version 410

layout(location=0) in vec4 Position;
layout(location=1) in vec2 Texcoord;

out vec2 vTexcoord;

void main()
{
	vTexcoord = Texcoord;
	gl_Position = Position;
}