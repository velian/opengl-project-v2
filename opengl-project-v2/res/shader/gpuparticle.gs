#version 410

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

in vec3 vPosition[];
in float vLifetime[];
in float vLifespan[];

out vec4 Colour;
out vec2 vTexCoords;

uniform int CurrentFrame;

uniform float FrameWidth;
uniform float FrameHeight;

uniform mat4 ProjectionView;
uniform mat4 CameraTransform;

uniform float StartSize;
uniform float EndSize;

uniform vec4 StartColour;
uniform vec4 EndColour;

void main()
{

	Colour = mix(StartColour, EndColour, vLifetime[0] / vLifespan[0]);

	float halfSize = mix(StartSize, EndSize, vLifetime[0] / vLifespan[0]) * 0.5f;

	vec3 corners[4];
	corners[0] = vec3(halfSize, -halfSize, 0);
	corners[1] = vec3(halfSize, halfSize, 0);
	corners[2] = vec3(-halfSize, -halfSize, 0);
	corners[3] = vec3(-halfSize, halfSize, 0);

	vec3 zAxis = normalize(CameraTransform[3].xyz - vPosition[0]);
	vec3 xAxis = cross(CameraTransform[1].xyz, zAxis);
	vec3 yAxis = cross(zAxis, xAxis);
	mat3 billboard = mat3(xAxis, yAxis, zAxis);

	vTexCoords = vec2((FrameWidth * CurrentFrame), 1);
	gl_Position = ProjectionView * vec4(billboard * corners[0] + vPosition[0], 1);
	EmitVertex();

	vTexCoords = vec2((FrameWidth * CurrentFrame), 0);
	gl_Position = ProjectionView * vec4(billboard * corners[1] + vPosition[0], 1);
	EmitVertex();

	vTexCoords = vec2(FrameWidth * CurrentFrame - FrameWidth,1);
	gl_Position = ProjectionView * vec4(billboard * corners[2] + vPosition[0], 1);
	EmitVertex();

	vTexCoords = vec2(FrameWidth * CurrentFrame - FrameWidth,0);
	gl_Position = ProjectionView * vec4(billboard * corners[3] + vPosition[0], 1);
	EmitVertex();
}