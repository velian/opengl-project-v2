#version 410

in vec2 vTexcoord;

out vec4 FragColor;

uniform sampler2D diffuse;

void main()
{
	FragColor = texture(diffuse, vTexcoord);

	if(FragColor.a < 0.5f)
	{
		discard;
	}
}