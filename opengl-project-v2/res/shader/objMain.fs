#version 410
in vec3 vPosition;
in vec2 vUV;
in vec3 vNormal;
in vec4 vShadowCoord;
flat in int vHasShadow;

out vec4 FragColor; 

uniform vec3 CameraPos;

uniform bool HasTexture;
uniform bool HasLight;

uniform float SpecPow; 
uniform float shadowBias;

uniform sampler2D diffuse;
uniform sampler2D shadowMap;

uniform vec3 LightColour;
uniform vec3 LightDir;
uniform vec3 ShadowColour; 

void main()
{
	float d;
	vec3 E;
	vec3 R;
	float s;

	if(HasLight && HasTexture)
	{
		d = max(0, dot(normalize(vNormal.xyz), LightDir));

		E = normalize(CameraPos - vPosition.xyz); 
		R = reflect( -LightDir, vNormal.xyz); 
		s = max (0.00001, dot(E, R));
		s = pow(s, SpecPow);

		FragColor = texture(diffuse, vUV) * vec4(LightColour * s, 1);

		//FragColor = texture(diffuse, vUV);
	}

	if(vHasShadow == 1)
	{
		if(texture(shadowMap, vShadowCoord.xy).r < vShadowCoord.z - shadowBias)
		{
			FragColor = FragColor * vec4(ShadowColour, 1);
		}
	}

	//if(HasLight)
	//{
	//	FragColor = FragColor * vec4(LightColour, 1);
	//}

	
	//s = pow(s, SpecPow); 
	//FragColor = /*texture(diffuse, vUV) * */vec4(LightColour * s, 1);// * texture(overlay, vUV);
}