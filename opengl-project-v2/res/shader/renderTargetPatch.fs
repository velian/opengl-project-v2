#version 410

in vec2 vTexcoord;

out vec4 FragColor;

uniform sampler2D diffuse;

vec4 Patch()
{
	vec2 texel = 0.1 / textureSize(diffuse, 0);

	vec4 colour = texture(diffuse, vTexcoord) * (11 / 3);
	colour += texture(diffuse, vTexcoord + vec2(0, texel.y)) * (-2 / 3);
	colour += texture(diffuse, vTexcoord - vec2(0, texel.y)) * (-2 / 3);
	colour += texture(diffuse, vTexcoord + vec2(texel.x, 0)) * (-3 / 3);
	colour += texture(diffuse, vTexcoord - vec2(texel.x, 0)) * (-3 / 3);

	return colour;
}

void main()
{
	FragColor = Patch();

	if(FragColor.a < 0.5f)
	{
		discard;
	}
}