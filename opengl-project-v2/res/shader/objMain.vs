#version 410

layout(location=0) in vec3 Position; 
layout(location=1) in vec3 Normal; 
layout(location=2) in vec2 UV; 

out vec3 vPosition; 
out vec2 vUV; 
out vec3 vNormal; 
out vec4 vShadowCoord;
flat out int vHasShadow;

uniform int HasShadow;

uniform mat4 ProjectionView; 
uniform mat4 ShadowMatrix;

void main()
{
	if(HasShadow == 1)
	{

		vShadowCoord = ShadowMatrix * vec4(Position, 1);
	}	

	vPosition = Position; 
	vUV = UV;
	vNormal = Normal.xyz;
	vHasShadow = HasShadow;
	gl_Position = ProjectionView * vec4(Position, 1);

	
}